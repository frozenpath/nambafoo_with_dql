<?php

namespace AppBundle\Repository;

/**
 * PurchaseRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PurchaseRepository extends \Doctrine\ORM\EntityRepository
{
    public function getPopularDishes($shop, $date)
    {
        return $this
            ->createQueryBuilder('h')
            ->select(array('d.name as dish_name', 'sum(h.qty) as qty_dishes', 's.name as shop_name'))
            ->innerJoin('h.dish', 'd')
            ->leftJoin('d.shop', 's')
            ->where('h.orderDate > :date')
            ->andWhere('d.shop = :shop')
            ->groupBy('d.name')
            ->having('qty_dishes > 10')
            ->orderBy('qty_dishes', 'desc')
            ->setMaxResults('2')
            ->setParameter('shop', $shop)
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }

}
