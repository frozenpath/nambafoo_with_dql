<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Dish;
use AppBundle\Entity\Purchase;
use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $shop1 = new Shop();
        $shop2 = new Shop();
        $shop3 = new Shop();
        $shop4 = new Shop();

        $shop1
            ->setName('Империя Пиццы')
            ->setDescription('Европейская, азиатская, японская и итальянская кухня. Кальяны и разнообразный бар.')
            ->setShopImage('597b8c2ed53d1.png');

        $shop2
            ->setName('Sarya')
            ->setDescription('Кафе «Sarya» находится в южной части Бишкека. Мы открыты для вас 24 часа в сутки. 
            Приятный интерьер, великолепные напитки и кухня, ненавязчивое обслуживание, а также замечательный 
            контингент нашего заведения помогут Вам расслабится и отвлечься от стремительного жизненного потока с 
            постоянно несущимися на нас событиями. В этой жизни у каждого обязательно должно быть место, где можно 
            все эти события спокойно взвесить и переосмыслить, а возможно с кем-нибудь и обсудить. «Sarya» — 
            как раз одно из таких мест.')
            ->setShopImage('597b8bc6076c4.jpg');

        $shop3
            ->setName('Adriano Coffee')
            ->setDescription('Сеть кофеен Adriano. Уютные и комфортные места для развлечений и 
            проведения деловых переговоров.')
            ->setShopImage('597b8deb6d536.jpg');

        $shop4
            ->setName('Astoria')
            ->setDescription('Astoria - кафе премиум класса. Пн-Вс 10:00-00:00. 
            ул. Московская 150, пересекает Коенкозова.')
            ->setShopImage('597b8d0946504.jpg');

        $admin = new User();
        $admin
            ->setEmail('admin@admin.ru')
            ->setUsername('admin')
            ->setRoles(['ROLE_ADMIN'])
            ->setEnabled(true)
            ->setName('Злой Админ');

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($admin, '123');
        $admin->setPassword($password);

        $dish1 = new Dish();
        $dish2 = new Dish();
        $dish3 = new Dish();
        $dish4 = new Dish();
        $dish5 = new Dish();
        $dish6 = new Dish();
        $dish7 = new Dish();

        $dish1
            ->setName('Рис')
            ->setDescription('Белоснежный рис')
            ->setPrice('100')
            ->setShop($shop1);

        $dish2
            ->setName('Кальян')
            ->setDescription('Дымный кальян')
            ->setPrice('400')
            ->setShop($shop1);

        $dish3
            ->setName('Селедка')
            ->setDescription('Еще живая селедка')
            ->setPrice('200')
            ->setShop($shop1);

        $dish4
            ->setName('Чай зеленый')
            ->setDescription('Грин теа')
            ->setPrice('50')
            ->setShop($shop3);

        $dish5
            ->setName('Пицца')
            ->setDescription('Большая и вкусная')
            ->setPrice('500')
            ->setShop($shop3);

        $dish6
            ->setName('Блюдо от шефа')
            ->setDescription('Секретные ингридиенты')
            ->setPrice('5000')
            ->setShop($shop3);

        $dish7
            ->setName('Чай черный')
            ->setDescription('Чьиорные глаза')
            ->setPrice('50')
            ->setShop($shop4);

        $purchase1 = new Purchase();
        $purchase2 = new Purchase();
        $purchase3 = new Purchase();
        $purchase4 = new Purchase();
        $purchase5 = new Purchase();
        $purchase6 = new Purchase();
        $purchase7 = new Purchase();

        $purchase1
            ->setOrderDate(new \DateTime())
            ->setQty('11')
            ->setDish($dish1);

        $purchase2
            ->setOrderDate(new \DateTime())
            ->setQty('3')
            ->setDish($dish1);

        $purchase3
            ->setOrderDate(new \DateTime())
            ->setQty('14')
            ->setDish($dish2);

        $purchase4
            ->setOrderDate(new \DateTime())
            ->setQty('333')
            ->setDish($dish3);

        $purchase5
            ->setOrderDate(new \DateTime())
            ->setQty('2')
            ->setDish($dish4);

        $purchase6
            ->setOrderDate(new \DateTime())
            ->setQty('41')
            ->setDish($dish5);

        $purchase7
            ->setOrderDate(new \DateTime())
            ->setQty('15')
            ->setDish($dish7);

        $manager->persist($shop1);
        $manager->persist($shop2);
        $manager->persist($shop3);
        $manager->persist($shop4);
        $manager->persist($admin);
        $manager->persist($dish1);
        $manager->persist($dish2);
        $manager->persist($dish3);
        $manager->persist($dish4);
        $manager->persist($dish5);
        $manager->persist($dish6);
        $manager->persist($dish7);
        $manager->persist($purchase1);
        $manager->persist($purchase2);
        $manager->persist($purchase3);
        $manager->persist($purchase4);
        $manager->persist($purchase5);
        $manager->persist($purchase6);
        $manager->persist($purchase7);

        $manager->flush();
    }
}
